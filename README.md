# LinkList

A Django app to display and edit a list of links

## What is this for?

This is an application to display a list of links.  The links can be collected into groups.  Both Links and Groups can be rearranged.

## Why make this?

1. I wanted something to act as my home page, showing me my frequently used links.
2. I wanted to show off that I have at least some Django skills.

The version I use everyday lives on my Raspberry Pi, but there is also a version deployed to Heroku at https://linklister.herokuapp.com/

## Deploying your own copy to Heroku

1. Create a project on [Heroku](https://www.heroku.com/)
2. Clone this repo
3. Install Heroku CLI if you have not already done so.
4. Login to Heroku CLI `heroku login`
5. Set up a Heroku remote `heroku git:remote -a <your heroku project>`
6. Add a SECRET_KEY to your Heroku project
    - In your Heroku project, navigate to Settings
    - Click the "Reveal Config Vars" button
    - Type SECRET_KEY in the Key field, and a long string in the Value field.
    - Click the Add button
6. Deploy to Heroku `git push heroku master` (Or if you're deploying from a different branch `git push heroku <yourbranch>:master`)
7. Create a user `heroku run python manage.py createsuperuser`

## Development Tasks
* Make the Edit UI less ugly
* Make favicons editable from the Edit UI (and not just the admin console)
* Make the link groups float horizontally, and generally clean up formatting
* Remove the heroku references
* Include a Dockerfile?
* Consider removing django-bootstrap-customizer (depends on `libsass`, which is a pain for containers)
