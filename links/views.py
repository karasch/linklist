from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth.decorators import login_required

from .models import Link, LinkGroup
from .forms import LinkForm, GroupForm

# Create your views here.

class IndexView(generic.ListView):
    template_name = "links/link_list.html"
    context_object_name = "groups_list"

    def get_queryset(self):
        return LinkGroup.objects.all()

@login_required
def edit_list_view(request):
    if request.method == 'POST':
        if 'add_link' in request.POST:
            form = LinkForm(request.POST)
            if form.is_valid():
                form.save()
                #return redirect('links:success')
        if 'add_group' in request.POST:
            form = GroupForm(request.POST)
            if form.is_valid():
                form.save()

    return render(request, 'links/edit_list.html', {'groups_list': LinkGroup.objects.all(), 'link_form': LinkForm(), 'group_form': GroupForm()})

def success(request):
    return HttpResponse('Link Added.')

@login_required
def delete_link(request, pk):
    return render(request, 'links/delete_link.html', {'link': Link.objects.get(pk=pk)})

@login_required
def delete_link_confirm(request, pk):
    Link.objects.get(pk=pk).delete()
    return redirect('links:editlist')

@login_required
def delete_group(request, pk):
    return render(request, 'links/delete_group.html', {'group': LinkGroup.objects.get(pk=pk)})

@login_required
def delete_group_confirm(request, pk):
    LinkGroup.objects.get(pk=pk).delete()
    return redirect('links:editlist')

@login_required
def move_link_up(request, pk):
    #find the link we're looking for
    moving_link = Link.objects.get(pk=pk)
    # get its link_order
    moving_link_order = moving_link.link_order
    # and group
    moving_link_group = moving_link.group
    # look up links with the same group and a lower link_order
    links = Link.objects.filter(group=moving_link_group, link_order__lt=moving_link_order)
    if links.count() == 0:
        # If there aren't any, error out
        #return HttpResponse(f"No links above {moving_link_order}")
        return redirect('links:editlist')
    else:
        # If there's at least one, get the last one
        above_link = links.last()
        # Swap the link_orders, and save the links.
        moving_link.link_order = above_link.link_order
        moving_link.save()
        above_link.link_order = moving_link_order
        above_link.save()
        return redirect('links:editlist')

@login_required
def move_link_down(request, pk):
    moving_link = Link.objects.get(pk=pk)
    moving_link_order = moving_link.link_order
    moving_link_group = moving_link.group
    links = Link.objects.filter(group=moving_link_group, link_order__gt=moving_link_order)
    if links.count() == 0:
        #return HttpResponse(f"No links below {moving_link_order}")
        return redirect('links:editlist')
    else:
        above_link = links.first()
        moving_link.link_order = above_link.link_order
        moving_link.save()
        above_link.link_order = moving_link_order
        above_link.save()
        return redirect('links:editlist')

@login_required
def move_group_up(request, pk):
    moving_group = LinkGroup.objects.get(pk=pk)
    moving_group_order = moving_group.group_order
    groups = LinkGroup.objects.filter(group_order__lt=moving_group_order)
    if groups.count() == 0:
        return redirect('links:editlist')
    else:
        above_group = groups.last()
        moving_group.group_order = above_group.group_order
        moving_group.save()
        above_group.group_order = moving_group_order
        above_group.save()
        return redirect('links:editlist')

@login_required
def move_group_down(request, pk):
    moving_group = LinkGroup.objects.get(pk=pk)
    moving_group_order = moving_group.group_order
    groups = LinkGroup.objects.filter(group_order__gt=moving_group_order)
    if groups.count() == 0:
        return redirect('links:editlist')
    else:
        above_group = groups.first()
        moving_group.group_order = above_group.group_order
        moving_group.save()
        above_group.group_order = moving_group_order
        above_group.save()
        return redirect('links:editlist')
