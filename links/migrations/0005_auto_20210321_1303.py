# Generated by Django 3.1.7 on 2021-03-21 18:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('links', '0004_auto_20210316_2104'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='link',
            options={'ordering': ['link_order']},
        ),
        migrations.AlterModelOptions(
            name='linkgroup',
            options={'ordering': ['group_order']},
        ),
        migrations.AlterField(
            model_name='link',
            name='group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='links.linkgroup'),
        ),
    ]
