from django.db import migrations
from urllib.parse import urlparse

def default_favicon_urls(apps, schema_editor):
    Link = apps.get_model('links', 'Link')
    for link in Link.objects.all():
        link.favicon = f"https://www.google.com/s2/favicons?domain={ urlparse(link.url).netloc }&sz=32"
        link.save()

def undo_favicon_urls(apps, schema_editor):
    Link = apps.get_model('links', 'Link')
    for link in Link.objects.all():
        link.favicon = ""
        link.save()

class Migration(migrations.Migration):

    dependencies = [
        ('links', '0006_link_favicon')
    ]

    operations = [
        migrations.RunPython(default_favicon_urls, reverse_code=undo_favicon_urls)
    ]
