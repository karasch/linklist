# Generated by Django 4.1.3 on 2022-12-24 17:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('links', '0005_auto_20210321_1303'),
    ]

    operations = [
        migrations.AddField(
            model_name='link',
            name='favicon',
            field=models.CharField(default='', max_length=2048),
        ),
    ]
