from django import forms
from .models import *

class LinkForm(forms.ModelForm):
    class Meta:
        model = Link
        fields = ['url', 'text', 'group']

class GroupForm(forms.ModelForm):
    class Meta:
        model = LinkGroup
        fields = ['group_name',]
