from django.db import models
from urllib.parse import urlparse

# Create your models here.

def get_group_order():
    """Get the largest used number from LinkGroup order_by
       Return 0 if there are no groups"""
    largest = LinkGroup.objects.all().order_by('group_order').last()
    if not largest:
        return 0
    else:
        return largest.group_order+1

def get_link_order():
    """Get the largest used number from LinkGroup order_by
    Return 0 if there are no groups"""
    largest = Link.objects.all().order_by('link_order').last()
    if not largest:
        return 0
    else:
        return largest.link_order+1

class LinkGroup(models.Model):
    group_name = models.CharField(max_length=256)
    group_order = models.IntegerField(default=get_group_order)

    def __str__(self):
        return f"{self.group_name}"

    class Meta:
        """Order LinkGroups by this field instead of by id, which is the default"""
        ordering = ['group_order']

class Link(models.Model):
    group = models.ForeignKey(LinkGroup, blank=True, null=True, on_delete=models.CASCADE)
    url = models.CharField(max_length=2048)
    text = models.CharField(max_length=256)
    favicon = models.CharField(max_length=2048, default="")
    link_order = models.IntegerField(default=get_link_order)

    def __str__(self):
        return f"{self.text} {self.url}"

    def get_domain(self):
        return urlparse(self.url).netloc

    def save(self, *args, **kwargs):
        if self.favicon == "":
            self.favicon = f"https://www.google.com/s2/favicons?domain={ self.get_domain() }&sz=32"
        super().save(*args, **kwargs)

    class Meta:
        """Order Links by this field"""
        ordering = ['link_order']
