from django.urls import path

from . import views

app_name = 'links'
urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('editlist', views.edit_list_view, name="editlist"),
    path('success', views.success, name='success'),
    path('<pk>/delete', views.delete_link, name='delete_link'),
    path('<pk>/deletelinkconfirm', views.delete_link_confirm, name='deletelinkconfirm'),
    path('<pk>/deletegroup', views.delete_group, name='delete_group'),
    path('<pk>/deletegroupconfirm', views.delete_group_confirm, name='deletegroupconfirm'),
    path('<pk>/movelinkup', views.move_link_up, name='movelinkup'),
    path('<pk>/movelinkdown', views.move_link_down, name='movelinkdown'),
    path('<pk>/movegroupup', views.move_group_up, name='movegroupup'),
    path('<pk>/movegroupdown', views.move_group_down, name='movegroupdown'),
]
